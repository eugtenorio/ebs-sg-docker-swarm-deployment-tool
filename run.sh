#!/bin/bash

export EBS_SG_DATA_DIR=`pwd`

## Load environment variables containing parameters for ebs-sg
env $(cat env | grep ^[A-Z] | xargs) \
    docker stack deploy --with-registry-auth -c docker-compose.yml ebs-sg
